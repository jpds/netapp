all:
	#cargo build --all-features
	cargo build
	cargo build --example fullmesh
	cargo build --all-features --example basalt
	RUST_LOG=netapp=trace,fullmesh=trace cargo run --example fullmesh -- -n 3242ce79e05e8b6a0e43441fbd140a906e13f335f298ae3a52f29784abbab500 -p 6c304114a0e1018bbe60502a34d33f4f439f370856c3333dda2726da01eb93a4894b7ef7249a71f11d342b69702f1beb7c93ec95fbcf122ad1eca583bb0629e7
	#RUST_LOG=netapp=debug,fullmesh=debug cargo run --example fullmesh

test:
	cargo test --all-features -- --test-threads 1
